# How to get value from ble, like Smart Scale or Oxymeter Keren

## 1. Run Bluetoothctl
``pi@atm:~ $ `` ``bluetoothctl``
then, you're on bluetoothctl environment
## 2. Observe the Bluetooth Controller
``[bluetooth]#`` ``show``
respond :
```js
[bluetooth]# show
Controller B8:27:EB:92:FD:F3
	Name: atm
	Alias: atm
	Class: 0x000000
	Powered: no
	Discoverable: no
	Pairable: no
	UUID: Headset AG                (00001112-0000-1000-8000-00805f9b34fb)
	UUID: Generic Attribute Profile (00001801-0000-1000-8000-00805f9b34fb)
	UUID: A/V Remote Control        (0000110e-0000-1000-8000-00805f9b34fb)
	UUID: Generic Access Profile    (00001800-0000-1000-8000-00805f9b34fb)
	UUID: PnP Information           (00001200-0000-1000-8000-00805f9b34fb)
	UUID: A/V Remote Control Target (0000110c-0000-1000-8000-00805f9b34fb)
	UUID: Audio Source              (0000110a-0000-1000-8000-00805f9b34fb)
	UUID: Handsfree Audio Gateway   (0000111f-0000-1000-8000-00805f9b34fb)
	Modalias: usb:v1D6Bp0246d052B
	Discovering: no
[bluetooth]#
```
## 3. Turn On Bluetooth Controller
``[bluetooth]#`` ``power on``
respond :
```js
[CHG] Controller B8:27:EB:92:FD:F3 Class: 0x480000
Changing power on succeeded
[CHG] Controller B8:27:EB:92:FD:F3 Powered: yes
[bluetooth]# show
Controller B8:27:EB:92:FD:F3
	Name: atm
	Alias: atm
	Class: 0x480000
	Powered: yes
	Discoverable: no
	Pairable: no
	UUID: Headset AG                (00001112-0000-1000-8000-00805f9b34fb)
	UUID: Generic Attribute Profile (00001801-0000-1000-8000-00805f9b34fb)
	UUID: A/V Remote Control        (0000110e-0000-1000-8000-00805f9b34fb)
	UUID: Generic Access Profile    (00001800-0000-1000-8000-00805f9b34fb)
	UUID: PnP Information           (00001200-0000-1000-8000-00805f9b34fb)
	UUID: A/V Remote Control Target (0000110c-0000-1000-8000-00805f9b34fb)
	UUID: Audio Source              (0000110a-0000-1000-8000-00805f9b34fb)
	UUID: Handsfree Audio Gateway   (0000111f-0000-1000-8000-00805f9b34fb)
	Modalias: usb:v1D6Bp0246d052B
	Discovering: no
[bluetooth]#
```
## 4. Scan to discover the Device
``[bluetooth]#`` ``scan on``
respond :
```js
[bluetooth]# scan on
Discovery started
[CHG] Controller B8:27:EB:92:FD:F3 Discovering: yes
[NEW] Device C2:00:E2:00:00:78 CBT112000120
[NEW] Device C2:00:E2:00:00:77 CBT111900119
[NEW] Device C2:00:E2:00:00:76 CBT111800118
[CHG] Device FE:76:0E:F3:0A:EF RSSI: -61
[CHG] Device FE:76:0E:F3:0A:EF ManufacturerData Key: 0x0157
[CHG] Device FE:76:0E:F3:0A:EF ManufacturerData Value: 0x00
[CHG] Device FE:76:0E:F3:0A:EF ManufacturerData Value: 0xb0
[CHG] Device FE:76:0E:F3:0A:EF ManufacturerData Value: 0xbc
[CHG] Device FE:76:0E:F3:0A:EF ManufacturerData Value: 0x67
[CHG] Device FE:76:0E:F3:0A:EF ManufacturerData Value: 0xb5
[CHG] Device FE:76:0E:F3:0A:EF ManufacturerData Value: 0x23
[CHG] Device FE:76:0E:F3:0A:EF ManufacturerData Value: 0x1c
[CHG] Device FE:76:0E:F3:0A:EF ManufacturerData Value: 0x72
[CHG] Device FE:76:0E:F3:0A:EF ManufacturerData Value: 0xef
[CHG] Device FE:76:0E:F3:0A:EF ManufacturerData Value: 0x5b
[CHG] Device FE:76:0E:F3:0A:EF ManufacturerData Value: 0x07
[CHG] Device FE:76:0E:F3:0A:EF ManufacturerData Value: 0x78
[CHG] Device FE:76:0E:F3:0A:EF ManufacturerData Value: 0x61
[CHG] Device FE:76:0E:F3:0A:EF ManufacturerData Value: 0xda
[CHG] Device FE:76:0E:F3:0A:EF ManufacturerData Value: 0x7a
[CHG] Device FE:76:0E:F3:0A:EF ManufacturerData Value: 0x30
[CHG] Device FE:76:0E:F3:0A:EF ManufacturerData Value: 0xec
[CHG] Device FE:76:0E:F3:0A:EF ManufacturerData Value: 0x03
[CHG] Device FE:76:0E:F3:0A:EF ManufacturerData Value: 0xfe
[CHG] Device FE:76:0E:F3:0A:EF ManufacturerData Value: 0x76
[CHG] Device FE:76:0E:F3:0A:EF ManufacturerData Value: 0x0e
[CHG] Device FE:76:0E:F3:0A:EF ManufacturerData Value: 0xf3
[CHG] Device FE:76:0E:F3:0A:EF ManufacturerData Value: 0x0a
[CHG] Device FE:76:0E:F3:0A:EF ManufacturerData Value: 0xef
[CHG] Device C2:00:E2:00:00:77 RSSI: -71
[CHG] Device C2:00:E2:00:00:77 TxPower: 0
[CHG] Device C2:00:E2:00:00:77 ServiceData Key: 0000fff0-0000-1000-8000-00805f9b34fb
[CHG] Device C2:00:E2:00:00:77 ServiceData Value: 0x64
[CHG] Device C2:00:E2:00:00:77 ServiceData Value: 0x00
[CHG] Device C2:00:E2:00:00:77 ServiceData Value: 0xde
[CHG] Device C2:00:E2:00:00:77 ServiceData Value: 0x00
[CHG] Device C2:00:E2:00:00:77 ServiceData Value: 0xdb
[NEW] Device 98:D3:91:FD:3C:B8 HC-05
[CHG] Device D3:8A:C5:8F:4F:44 RSSI: -82
[CHG] Device D3:8A:C5:8F:4F:44 ServiceData Key: 0000181b-0000-1000-8000-00805f9b34fbc
```
## 5. Show info the Device
``[bluetooth]#`` ``info <Mac-Address>``
example :
``[bluetooth]#`` ``info D3:8A:C5:8F:4F:44``
respond :
```js
[bluetooth]# info D3:8A:C5:8F:4F:44
Device D3:8A:C5:8F:4F:44
	Name: MIBCS
	Alias: MIBCS
	Appearance: 0x0c80
	Paired: no
	Trusted: no
	Blocked: no
	Connected: no
	LegacyPairing: no
	UUID: Vendor specific           (00001530-0000-3512-2118-0009af100700)
	UUID: Generic Access Profile    (00001800-0000-1000-8000-00805f9b34fb)
	UUID: Generic Attribute Profile (00001801-0000-1000-8000-00805f9b34fb)
	UUID: Device Information        (0000180a-0000-1000-8000-00805f9b34fb)
	UUID: Body Composition          (0000181b-0000-1000-8000-00805f9b34fb)
	Modalias: bluetooth:v0157p0006d0000
	RSSI: -64
```
MIBCS is the intended device.
## 6. Pair the Deivce
``[bluetooth]#`` ``pair <Mac-Address>``
example :
``[bluetooth]#`` ``pair D3:8A:C5:8F:4F:44``
respond :
```js
[bluetooth]# pair D3:8A:C5:8F:4F:44
Attempting to pair with D3:8A:C5:8F:4F:44
[CHG] Device D3:8A:C5:8F:4F:44 Connected: yes
```
## 7. Set Discovrable On (optional)
``[bluetooth]#`` ``discoverable on``
respond :
```js
[bluetooth]# discoverable on
Changing discoverable on succeeded
[CHG] Controller B8:27:EB:92:FD:F3 Discoverable: yes
```
## 8. Connect to device
``[bluetooth]#`` ``connect <Mac-Address>``
example
``[bluetooth]#`` ``connect D3:8A:C5:8F:4F:44``
respond :
```js
[bluetooth]# connect D3:8A:C5:8F:4F:44
Attempting to connect to D3:8A:C5:8F:4F:44
[CHG] Device 98:D3:91:FD:3C:B8 RSSI: -92
[CHG] Device D3:8A:C5:8F:4F:44 Connected: yes
Connection successful
```
After that.. 
then, you're connected and  on that device's environment or MIBCS
## 9. Trust that device (optional)
``[bluetooth]#`` ``trust <Mac-Address>``
example
``[bluetooth]#`` ``trust D3:8A:C5:8F:4F:44``
respond :
```js
[MIBCS]# trust D3:8A:C5:8F:4F:44
[CHG] Device D3:8A:C5:8F:4F:44 Trusted: yes
Changing D3:8A:C5:8F:4F:44 trust succeeded
```
## 10. Show the list of characteristic
``[MIBCS]# `` ``list-attributes``
respond
```js
[MIBCS]# list-attributes
Primary Service
	/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service000c
	00001801-0000-1000-8000-00805f9b34fb
	Generic Attribute Profile
Characteristic
	/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service000c/char000d
	00002a05-0000-1000-8000-00805f9b34fb
	Service Changed
Descriptor
	/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service000c/char000d/desc000f
	00002902-0000-1000-8000-00805f9b34fb
	Client Characteristic Configuration
Primary Service
	/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service0010
	0000180a-0000-1000-8000-00805f9b34fb
	Device Information
Characteristic
	/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service0010/char0011
	00002a25-0000-1000-8000-00805f9b34fb
	Serial Number String
Characteristic
	/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service0010/char0013
	00002a28-0000-1000-8000-00805f9b34fb
	Software Revision String
Characteristic
	/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service0010/char0015
	00002a27-0000-1000-8000-00805f9b34fb
	Hardware Revision String
Characteristic
	/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service0010/char0017
	00002a23-0000-1000-8000-00805f9b34fb
	System ID
Characteristic
	/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service0010/char0019
	00002a50-0000-1000-8000-00805f9b34fb
	PnP ID
Primary Service
	/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b
	0000181b-0000-1000-8000-00805f9b34fb
	Body Composition
Characteristic
	/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char001c
	00002a2b-0000-1000-8000-00805f9b34fb
	Current Time
Characteristic
	/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char001e
	00002a9b-0000-1000-8000-00805f9b34fb
	Body Composition Feature
Characteristic
	/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020
	00002a9c-0000-1000-8000-00805f9b34fb
	Body Composition Measurement
Descriptor
	/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020/desc0022
	00002902-0000-1000-8000-00805f9b34fb
	Client Characteristic Configuration
Characteristic
	/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0023
	00002a2f-0000-3512-2118-0009af100700
	Vendor specific
Descriptor
	/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0023/desc0025
	00002902-0000-1000-8000-00805f9b34fb
	Client Characteristic Configuration
Primary Service
	/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service0026
	00001530-0000-3512-2118-0009af100700
	Vendor specific
Characteristic
	/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service0026/char0027
	00001531-0000-3512-2118-0009af100700
	Vendor specific
Descriptor
	/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service0026/char0027/desc0029
	00002902-0000-1000-8000-00805f9b34fb
	Client Characteristic Configuration
Characteristic
	/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service0026/char002a
	00001532-0000-3512-2118-0009af100700
	Vendor specific
Characteristic
	/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service0026/char002c
	00002a04-0000-1000-8000-00805f9b34fb
	Peripheral Preferred Connection Parameters
Descriptor
	/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service0026/char002c/desc002e
	00002902-0000-1000-8000-00805f9b34fb
	Client Characteristic Configuration
Characteristic
	/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service0026/char002f
	00001542-0000-3512-2118-0009af100700
	Vendor specific
Descriptor
	/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service0026/char002f/desc0031
	00002902-0000-1000-8000-00805f9b34fb
	Client Characteristic Configuration
Characteristic
	/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service0026/char0032
	00001543-0000-3512-2118-0009af100700
	Vendor specific
Descriptor
	/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service0026/char0032/desc0034
	00002902-0000-1000-8000-00805f9b34fb
	Client Characteristic Configuration
```
## 11. Observe every characteristic
``[MIBCS]# attribute-info`` ``/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020``
```js
[MIBCS:/service001b/char0020]# attribute-info /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020
Characteristic - Body Composition Measurement
	UUID: 00002a9c-0000-1000-8000-00805f9b34fb
	Service: /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b
	Value: 0x02
	Value: 0x24
	Value: 0xe3
	Value: 0x07
	Value: 0x06
	Value: 0x1c
	Value: 0x0e
	Value: 0x2c
	Value: 0x34
	Value: 0xfd
	Value: 0xff
	Value: 0x20
	Value: 0x03
	Notifying: no
	Flags: indicate
```
## 12. Select that characteristic
``[MIBCS]# select-attribute`` ``/org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020``
```js
[MIBCS]# select-attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020
[MIBCS:/service001b/char0020]#
```
Now, you're on that characteristic or Body Measuriment Characteristic
## 13. Notify On for get value as hex
``[MIBCS:/service001b/char0020]#`` ``notify on``
respond 
```js
[MIBCS:/service001b/char0020]# notify on
[CHG] Device C2:00:E2:00:00:78 RSSI: -83
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x02
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x04
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0xe3
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x07
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x06
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x1c
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x0e
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x34
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x1d
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x00
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x00
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0xf8
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x02
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x02
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x04
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0xe3
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x07
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x06
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x1c
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x0e
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x34
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x1e
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x00
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x00
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0xbc
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x02
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x02
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x04
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0xe3
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x07
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x06
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x1c
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x0e
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x34
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x1e
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x00
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x00
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0xa8
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x02
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x02
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x04
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0xe3
[CHG] Attribute /org/bluez/hci0/dev_D3_8A_C5_8F_4F_44/service001b/char0020 Value: 0x07
```
we recived hex value, then we can convert hex to ASCII
